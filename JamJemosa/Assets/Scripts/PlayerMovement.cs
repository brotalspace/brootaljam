﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    private Player _player;
    private Rigidbody _rigidbody;
    private Animator _animator;
    [SerializeField] private GameObject _aimSprite;

    [SerializeField] private float _speed = 2f;
    [SerializeField] private float _rotationSpeed = 20f;

    public static bool IsAiming { get; private set; } = false;
    private bool movingForward = false;

    private void Start()
    {
        _player = ReInput.players.GetPlayer(0);
        _rigidbody = this.GetComponent<Rigidbody>();
        _animator = this.GetComponentInChildren<Animator>();
        IsAiming = false;
    }

    private void Update()
    {
        if (_player.GetButtonDown("Aim"))
        {
            ChangeCamera(true);
            IsAiming = true;
            _animator.SetBool("Aim", true);
            _animator.SetLayerWeight(1, 1);
            _aimSprite.SetActive(true);
        }
        else if (_player.GetButtonUp("Aim"))
        {
            ChangeCamera(false);
            IsAiming = false;
            _animator.SetBool("Aim", false);
            _animator.SetLayerWeight(1, 0);
            _aimSprite.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        movingForward = false;
        _rigidbody.velocity = Vector3.zero;

        if (_player.GetAxisRaw("MoveVertical") > 0)
        {
            _rigidbody.velocity += this.transform.forward;
            movingForward = true;
        }
        else if (_player.GetAxisRaw("MoveVertical") < 0)
        {
            _rigidbody.velocity -= this.transform.forward;
        }

        if (_player.GetAxisRaw("MoveHorizontal") > 0)
        {
            _rigidbody.velocity += this.transform.right;
        }
        else if (_player.GetAxisRaw("MoveHorizontal") < 0)
        {
            _rigidbody.velocity -= this.transform.right;
        }

        _rigidbody.velocity = _rigidbody.velocity.normalized * _speed;

        _animator.SetFloat("Speed", _rigidbody.velocity.magnitude);

        RotatePlayerWithCam();
    }


    private void RotatePlayerWithCam()
    {
        if (!IsAiming)
        {
            if (_rigidbody.velocity.magnitude > 0.2f)
            {
                Vector3 cameraRotation = Camera.main.transform.rotation.eulerAngles;
                cameraRotation.x = 0;
                cameraRotation.z = 0;
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(cameraRotation), _rotationSpeed * Time.deltaTime);
            }
        }
    }

    public void ChangeCamera(bool change)
    {
        _animator.SetBool("Aim", change);
    }
}
