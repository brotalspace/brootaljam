﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class AttackSystem : MonoBehaviour
{
    private Animator _animator;
    private Player _player;

    private float _timer = 0f;

    private float _timebetweenShots = 0.5f;

    [SerializeField] private Transform _bulletPoint;
    [SerializeField] private GameObject _bullet;

    private void Awake()
    {
        _player = ReInput.players.GetPlayer(0);
        _animator = this.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (PlayerMovement.IsAiming == true)
        {
            if (_player.GetButtonDown("Attack"))
            {
                Attack();
            }
            else if (_player.GetButton("Attack"))
            {
                if (_timer <= 0f)
                {
                    Attack();
                }
                else
                {
                    _animator.ResetTrigger("Shoot");
                }
            }
        }

        _timer -= Time.deltaTime;
    }

    private void Attack()
    {
        _animator.SetTrigger("Shoot");
        _timer = _timebetweenShots;
    }
}
