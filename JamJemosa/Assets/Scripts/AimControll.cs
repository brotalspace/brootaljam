﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Cinemachine;

public class AimControll : MonoBehaviour
{
    [SerializeField] private int _playerId = 0;
    private Player _player;
    [SerializeField] private CinemachineFreeLook _freelookCamera;
    [SerializeField] private float _distance = -1.0f;
    [SerializeField] private float _rotationSpeed = 100f;

    private void Start()
    {
        _player = ReInput.players.GetPlayer(_playerId);
    }

    private void Update()
    {
        if (PlayerMovement.IsAiming)
        {
            float aux = _player.GetAxis("MouseX");
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(0, _rotationSpeed * aux, 0));
        }
    }

    private void RotatePlayer()
    {
        Vector3 cameraRotation = Camera.main.transform.rotation.eulerAngles;
        cameraRotation.x = 0;
        cameraRotation.z = 0;
        this.transform.rotation = Quaternion.Euler(cameraRotation);
    }
}
