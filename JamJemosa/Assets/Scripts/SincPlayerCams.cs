﻿using UnityEngine;
using Cinemachine;
using Rewired;

public class SincPlayerCams : MonoBehaviour
{
    [SerializeField] private CinemachineFreeLook _backCamera;
    [SerializeField] private CinemachineFreeLook _aimCamera;

    private Player _playerController;

    private bool _isAming = false;

    private void Awake()
    {
        _playerController = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        if (_playerController.GetButtonDown("Aim"))
        {
            _isAming = true;
        }
        else if (_playerController.GetButtonUp("Aim"))
        {
            _isAming = false;
        }

        if (_isAming)
        {
            _backCamera.m_XAxis.Value = _aimCamera.m_XAxis.Value;
        }
        else
        {
            _aimCamera.m_XAxis.Value = _backCamera.m_XAxis.Value;
        }
    }
}
