﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimIk : MonoBehaviour
{
    [SerializeField] private float _distance = 100f;
    private Animator _animator;
    private Vector3 _pointToLook;

    private void Awake()
    {
        _animator = this.GetComponent<Animator>();
        Matrix4x4 m = Camera.main.cameraToWorldMatrix;
        _pointToLook = m.MultiplyPoint(new Vector3(0, 0, _distance));
    }

    private void Update()
    {
        if (PlayerMovement.IsAiming)
        {
            Matrix4x4 m = Camera.main.cameraToWorldMatrix;
            _pointToLook = m.MultiplyPoint(new Vector3(0, 0, _distance));
        }
    }

    //private void FixedUpdate()
    //{
    //    if (PlayerMovement.IsAiming)
    //    {
    //        Matrix4x4 m = Camera.main.cameraToWorldMatrix;
    //        _pointToLook = m.MultiplyPoint(new Vector3(0, 0, _distance));
    //    }
    //}

    private void OnAnimatorIK(int layerIndex)
    {
        if (layerIndex == 1)
        {
            _animator.SetIKPosition(AvatarIKGoal.RightHand, _pointToLook);
            _animator.SetIKPosition(AvatarIKGoal.LeftHand, _pointToLook);
            _animator.SetLookAtPosition(_pointToLook);

            if (PlayerMovement.IsAiming == true)
            {
                _animator.SetLookAtWeight(1);
                _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                _animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            }
            else
            {
                _animator.SetLookAtWeight(0);
                _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                _animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
            }
        }
    }
}
