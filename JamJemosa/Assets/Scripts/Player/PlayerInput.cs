﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Rewired;

public class PlayerInput : MonoBehaviour
{
    [System.Flags] private enum Inputs
    {
        Null = 0,
        Move = 1 << 0,
        AimOn = 1 << 1,
        AimOff = 1 << 2,
        Attack = 1 << 3,
        Reload = 1 << 4,

        Aim = AimOn | AimOff
    }

    [BoxGroup("Atributes", showLabel: false)] [ShowInInspector] private readonly int _playerControllerId = 0;
    private Player _playerController;

    private Inputs _curentInputs = Inputs.Null;

    public Vector3 MoveDirection { get; private set; } = Vector3.zero;

    [SerializeField] [BoxGroup("Atributes/Controller Parameters")] private float _controllerDeadZone = 0.25f;

    private bool _aimOn = false;

    private void Awake()
    {
        _playerController = ReInput.players.GetPlayer(_playerControllerId);   
    }

    private void Update()
    {
        GetInputs();
    }

    private void GetInputs()
    {
        _curentInputs = Inputs.Null;

        GetMovement();
        GetAim();
    }

    private void GetMovement()
    {
        MoveDirection = new Vector3(_playerController.GetAxisRaw("MoveHorizontal"), 0f, _playerController.GetAxisRaw("MoveVertical"));

        if (Mathf.Abs(MoveDirection.magnitude) >= _controllerDeadZone)
        {
            _curentInputs |= Inputs.Move;
        }
    }

    private void GetAim()
    {
        if (_playerController.GetButtonDown("Aim"))
        {
            _curentInputs |= Inputs.AimOn;
        }
        else if (_playerController.GetButtonUp("Aim"))
        {
            _curentInputs |= Inputs.AimOff;
        }
    }

    private void GetAttack()
    {
        if (_playerController.GetButton("Attack") || _playerController.GetButtonDown("Attack"))
        {
            _curentInputs |= Inputs.Attack;
        }
    }
}
