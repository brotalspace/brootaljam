﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed = 1000f;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        Shoot();
    }

    private void Shoot()
    {
        _rigidbody.velocity = -this.transform.up * _speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        this._rigidbody.velocity = Vector3.zero;
        this._rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }
}
