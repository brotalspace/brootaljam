﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class FixMouseControll : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    [Button(Name = "Lock")]
    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    [Button(Name = "Unlock")]
    private void UnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
    }
}
